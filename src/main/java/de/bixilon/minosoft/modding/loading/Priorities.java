/*
 * Minosoft
 * Copyright (C) 2020 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.minosoft.modding.loading;

public enum Priorities {
    LOWEST,
    LOW,
    NORMAL,
    HIGH,
    HIGHEST,
    ULTRA_HIGH; // this priority is even higher. Do not use in normal case!

    private static final Priorities[] PRIORITIES = values();

    public static Priorities byId(int id) {
        return PRIORITIES[id];
    }

    public static Priorities getHigherPriority(Priorities priority) {
        return byId(priority.ordinal() + 1);
    }
}
